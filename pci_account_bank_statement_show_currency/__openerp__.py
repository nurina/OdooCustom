# -*- coding: utf-8 -*-
{
    'name'          : 'Account Show Amount Currency',
    'version'       : '1.0',
    'sequence'      : 1,
    'description'   : """

v1.0
----
show amount currency and currency field on bank statement line \n
Nur Inayati


                      """,
    'category'      : 'Generic Module',
    'depends'       : ['account'],
    'data'          : [
                       'views/account_journal_view.xml',                 
                      ],
    'installable'   : True,
    'application'   : True,
    'auto_install'  : False
}