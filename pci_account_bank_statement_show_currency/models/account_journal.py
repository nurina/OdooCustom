from openerp import models, api, _, fields
import logging
_log = logging.getLogger(__name__)


class account_journal(models.Model):
    _inherit = "account.journal"
    
    show_currency = fields.Boolean('Allow to Show Amount Currency', default=False, help="Allow to Show/Hide Amount Currency In Bank Statement")

    @api.multi
    def open_action(self):
        res = super(account_journal, self).open_action()
        ctx = res.get('context') or self._context.copy()
        ctx.update({
            'default_show_currency': self.show_currency
        })
        res.update({'context': ctx,})       
        return res

    @api.multi
    def create_bank_statement(self):
        res = super(account_journal, self).create_bank_statement()
        ctx = {
            'default_journal_id': self.id,
            'default_show_currency': self.show_currency,
        }
        res.update({'context': ctx,})        
        return res

    @api.multi
    def create_cash_statement(self):
        res = super(account_journal, self).create_cash_statement()
        ctx = res.get('context') or self._context.copy()
        ctx.update({
            'default_show_currency': self.show_currency
        })
        res.update({'context': ctx,})
        return res
