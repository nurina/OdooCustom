{
    'name': 'PCI Stock Receive Landed Cost',
    'version': '1.0',
    'author': 'Portcities Indonesia',
    'description': """
v1.0
--- Give warning when validate DO which need landed cost first \
*Author : Nur Inayati

    """,
    'website': 'http://www.portcities.net',
    'depends': ['stock', 'stock_landed_costs_average'],
    'category': 'Generic Module',
    'data': [
        'views/partner_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

