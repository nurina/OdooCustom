from openerp import models, fields, api, _
from openerp.exceptions import UserError, RedirectWarning, ValidationError
from openerp.exceptions import except_orm, Warning

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    is_landed_cost = fields.Boolean(string="Is Landed Cost")
    
    