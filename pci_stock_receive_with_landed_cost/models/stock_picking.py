from openerp import models, api, _
from openerp.exceptions import UserError


class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def do_new_transfer(self):
        procurement_group_ids = self.filtered(
            lambda pick: pick.group_id and \
            pick.group_id.partner_id.is_landed_cost
            and pick.picking_type_id.code == 'internal'
        ).mapped('group_id').ids
        incoming_picking_ids = self.search([
            ('group_id', 'in', procurement_group_ids),
            ('picking_type_id.code', '=', 'incoming'),
            ('state', '=', 'done')])
    
        landed_cost = self.env['stock.landed.cost'].search([(
            'picking_ids', 'in', incoming_picking_ids.ids), ('state', '=', 'done')])

        picking_attached = landed_cost.mapped('picking_ids')
        match_picking = incoming_picking_ids & picking_attached
        if not landed_cost and (procurement_group_ids or incoming_picking_ids):
            raise UserError(_("There is no Landed Cost to this shipment"))
        elif len(match_picking) != len(incoming_picking_ids):
            raise UserError(_("Landed Cost is not completed yet"))
        else:
            return super(StockPicking, self).do_new_transfer()

