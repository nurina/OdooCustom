# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import UserError
import re
import base64
import cStringIO
import xlsxwriter
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from cStringIO import StringIO


class AccountReportIntercompanySummary(models.TransientModel):
    _inherit = "account.common.account.report"
    _name = "account.intercompany.summary"
    _description = "Intercompany Summary Report"
    
    @api.model
    def default_get(self, fields):
        if self._context is None: self._context = {}
        res = super(AccountReportIntercompanySummary, self).default_get(fields)
        first_day_month = str(datetime.now() + relativedelta(months=-1, day=1))[:10]
        last_day_month = str(datetime.now() + relativedelta(months=0, day=1,days=-1))[:10]
        res.update({
            'date_from': first_day_month,
            'date_to': last_day_month,
            'domain_branch':'pluit',
        })
        return res
    @api.model
    def domain_intercompany_filter(self):
        return ['|',('branch_inter_src_id','=',self.env.user.partner_id.branch_id.id),('branch_inter_dest_id','=',self.env.user.partner_id.branch_id.id)]
    
    @api.onchange('analytic_account_id')    
    def onchange_account_id(self): 
        domain = {'account_intercomp_ids' : ['|',('branch_inter_src_id','=',self.analytic_account_id.partner_id.id),('branch_inter_dest_id','=',self.analytic_account_id.partner_id.id)]}
        return {'domain':domain}
    analytic_account_id = fields.Many2one('account.analytic.account',string='Analytic Account',default= lambda self: self.env.user.partner_id.branch_id.analytic_account_id,required = True)
    account_intercomp_ids = fields.Many2many('account.account',string='Intercompany Accounts',required = True,domain=domain_intercompany_filter)
    
    @api.multi
    def pre_print_report(self, data):
        res = super(AccountReportIntercompanySummary, self).pre_print_report(data)
        codes = self.analytic_account_id.name
        account_codes = ''
        if self.account_intercomp_ids:
            for account in self.account_intercomp_ids:
                if account_codes:
                    account_codes = account_codes+", "+account.name
                else:
                    account_codes = account.name
        active_user = self._uid
        user_id = self.env['res.users'].search([('id','=',active_user)])
        account_ids = [account.id for account in self.account_intercomp_ids]
        data['form'].update(self.read(['analytic_account_id'])[0])
        data['form'].update({'analytic_account_code':codes})
        data['form'].update({'account_codes':account_codes})
        data['form'].update({'account_ids':account_ids})
        data['form'].update({'user_active':user_id.name})
        return data
    
    def _print_report(self, data):
        data = self.pre_print_report(data)
        data['form'].update(self.read(['initial_balance', 'sortby'])[0])
        if data['form'].get('initial_balance') and not data['form'].get('date_from'):
            raise UserError(_("You must define a Start Date"))
        if data['form'].get('initial_balance') and not data['form'].get('date_to'):
            raise UserError(_("You must define a End Date"))
        records = self.env[data['model']].browse(data.get('ids', []))

        return self.env['report'].with_context(landscape=False).get_action(records, 'lc_intercompany_report.intercompany_summary', data=data)

    @api.multi
    def check_report(self):
        res = super(AccountReportIntercompanySummary, self).check_report()
        res['data']['form']['used_context'].update({'analytic_id': self.analytic_account_id.id or False})
        return res
    
    data_x   = fields.Binary(string="File", readonly=True)
    name     = fields.Char('Filename',size=100, readonly=True)
    state_x  = fields.Selection((('choose','choose'),('get','get'),), default='choose')
    initial_balance = fields.Boolean(string='Include Initial Balances',
                                    help='If you selected date, this field allow you to add a row to display the amount of debit/credit/balance that precedes the filter you\'ve set.',default=True)
    
    
    @api.multi     
    def intercompany_report(self):
        fp = StringIO()
        workbook = xlsxwriter.Workbook(fp)
        filename = 'Intercompany Summary.xlsx'
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'journal_ids', 'target_move'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        date_from = data['form']['used_context'].get('date_from')
        date_to = data['form']['used_context'].get('date_to')
        
        #### STYLE
        #################################################################################
        top_style = workbook.add_format({'bold': 1,'valign':'vcenter','align':'center'})
        top_style.set_font_name('Arial')
        top_style.set_font_size('15')
        top_style.set_text_wrap()
        #################################################################################
        normal_style1 = workbook.add_format({'valign':'vcenter'})
        normal_style1.set_border()
        normal_style1.set_font_name('Arial')
        normal_style1.set_font_size('11')
        #################################################################################
        header_style = workbook.add_format({'bold': 1,'align':'center','valign':'vcenter'})
        header_style.set_font_name('Arial')
        header_style.set_font_size('11')
        header_style.set_border()
        header_style.set_text_wrap()
        header_style.set_bg_color('#BDC3C7')
        #################################################################################
        normal_float = workbook.add_format({'valign':'vcenter'})
        normal_float.set_border()
        normal_float.set_text_wrap()
        normal_float.set_num_format('"Rp "#,##0.00')
        normal_float.set_font_name('Courier New')
        normal_float.set_font_size('12')
        #################################################################################
        bold = workbook.add_format({'bold': True})
        
        worksheet = workbook.add_worksheet("Intercompany Summary")
        worksheet.set_column('A:A', 10) 
        worksheet.set_column('B:B', 15) 
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 10)
        worksheet.set_column('E:E', 30)
        worksheet.set_column('F:F', 20)      
        worksheet.set_column('G:G', 20) 
        worksheet.set_column('H:H', 20)  

        worksheet.merge_range('A1:H1',self.company_id.name+': Intercompany Summary', top_style)        

        worksheet.set_row(0, 45)
        worksheet.set_row(2, 25)
        worksheet.set_row(3, 25)
        codes = ''
        if self.analytic_account_id:
            if codes:
                codes = codes+", "+self.analytic_account_id.name
            else:
                codes = self.analytic_account_id.name
        account_codes = ''
        if self.account_intercomp_ids:
            for account_id in self.account_intercomp_ids:
                if account_codes:
                    account_codes = account_codes+", "+account_id.name
                else:
                    account_codes = account_id.name
        dateFrom = datetime.strptime(date_from, "%Y-%m-%d")
        dateFrom = datetime.strftime(dateFrom, "%d-%m-%Y")
        dateTo = datetime.strptime(date_to, "%Y-%m-%d")
        dateTo = datetime.strftime(dateTo, "%d-%m-%Y")
        period = '%s - %s'%(dateFrom, dateTo)
        worksheet.write(2,0, 'Analytical Account:', header_style)
        worksheet.write(2,1, codes,)
        worksheet.write(3,0, 'Display Account:', header_style)
        worksheet.write(3,1, account_codes,)
        worksheet.write(2,3, 'Period:', header_style)
        worksheet.write(2,4, period,)
        worksheet.write(6,0, 'Date',header_style)
        worksheet.write(6,1, 'Partner',header_style)
        worksheet.write(6,2, 'Ref',header_style)
        worksheet.write(6,3, 'Type',header_style)
        worksheet.write(6,4, 'Source Document',header_style)
        worksheet.write(6,5, 'Debit',header_style)
        worksheet.write(6,6, 'Credit',header_style)
        worksheet.write(6,7, 'Balance',header_style)
        
        account_ids = self.env['report.lc_intercompany_report.intercompany_summary'].with_context(data['form'].get('used_context',{})).get_account_move_intercompany(self.analytic_account_id.id,self.account_intercomp_ids,self.initial_balance,date_from,date_to)
        row = 7
        for account in account_ids:
            worksheet.write(row,0, account['code'],bold)
            worksheet.write(row,1, account['name'],bold)
            worksheet.write(row,5, account['debit'],bold)
            worksheet.write(row,6, account['credit'],bold)
            worksheet.write(row,7, account['balance'],bold)
            row+=1
            for vals in account['move_lines']:
                date =''
                if vals['date']:
                    date = datetime.strptime(vals['date'], "%Y-%m-%d")
                    date = datetime.strftime(date, "%d/%m/%y")
                worksheet.write(row,0, date,normal_style1)
                worksheet.write(row,1, vals['partner'],normal_style1)
                worksheet.write(row,2, vals['ref'],normal_style1)
                worksheet.write(row,3, vals['type'],normal_style1)
                worksheet.write(row,4, vals['origin'] or vals['v_label'],normal_style1)
                worksheet.write(row,5, vals['debit'],normal_float)
                worksheet.write(row,6, vals['credit'],normal_float)
                worksheet.write(row,7, vals['balance'],normal_float)
                row+=1
        worksheet.set_paper(9)
        worksheet.set_landscape()
        worksheet.set_margins(0.5, 0.5, 0.5, 0.5)
        worksheet.center_horizontally()
        worksheet.fit_to_pages(1, 0)

        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'state_x':'get', 'data_x':out, 'name': filename})
        ir_model_data = self.pool.get('ir.model.data')
        fp.close()
        form_res = ir_model_data.get_object_reference(self._cr,self._uid, 'lc_intercompany_report', 'account_intercompany_summary_view')
        form_id = form_res and form_res[1] or False
    
        return {
            'name': _('Download XLS'),
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'account.intercompany.summary',
             'res_id': self.id,
             'view_id': False,
             'views': [(form_id, 'form')],
             'type': 'ir.actions.act_window',
             'target': 'new'
         }