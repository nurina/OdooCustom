# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import tools
from openerp import models, fields, api, _
from datetime import datetime,timedelta

class intercompany_report(models.Model):
    _name = "account.move.line.dropship"
    _description = "Journal Item Stock Move Dropship report Pivot"
    _auto = False

    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    qty = fields.Float('Quantity', readonly=True)
    debit = fields.Float(string="Debit", readonly=True)
    credit = fields.Float(string="Credit", readonly=True)
    value = fields.Float(string="Balance", readonly=True)
    label = fields.Char('Label')
    account = fields.Many2one('account.account', 'Account')
    cost_price = fields.Float('Cost Price', group_operator = 'avg')
    analytic_account_id = fields.Many2one('account.analytic.account','Analytic Account')
    ref = fields.Char('Reference')
    date = fields.Date('Journal Date')
    journal_id = fields.Many2one('account.journal','Journal')

    partner_id = fields.Many2one('res.partner', 'Partner')
    move_id = fields.Many2one('account.move', 'Journal Entry',readonly=True)
    branch_id = fields.Many2one('res.partner', "Branch")
    
    def _select(self):
        select_str = """
             SELECT  
             m.id as id,
             m.journal_id as journal_id, 
             m.name as label,
             m.product_id as product_id, 
             a.id as account, 
             sum(m.quantity) as qty , 
             m.related_price_unit as cost_price, 
             m.balance as value, 
             m.debit as debit, 
            m.credit as credit,
             m.analytic_account_id as analytic_account_id, 
             m.ref as ref,
             m.date as date,
             m.partner_id as partner_id,
             m.move_id as move_id,
            w.branch_id as branch_id
                          """ 
        return select_str
    
    def _from_ds(self):
        from_str = """
            sale_order s 
            left join stock_warehouse w ON s.warehouse_id = w.id 
            left join stock_picking p ON p.group_id = s.procurement_group_id
            left join account_move_line m on m.ref = p.name
            left join account_account a on a.id = m.account_id
            WHERE s.branch_id != w.branch_id and s.state != 'cancel'
            AND  m.journal_id='7' and m.debit > 0
            """
        return from_str
    

    def _group_by(self):
        group_by_str = """
            GROUP BY   
                m.id,
                m.journal_id , 
                 m.name,
                 m.product_id, 
                 a.id,  
                 m.related_price_unit , 
                 m.debit,
                 m.credit , 
                 m.balance , 
                 m.analytic_account_id , 
                 m.ref ,
                 m.date,
                 w.branch_id,
                 m.partner_id,
                 m.move_id
        """
        return group_by_str

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute("""CREATE or REPLACE VIEW %s as (
            %s 
                FROM  %s 
                %s
            )""" % (self._table, 
                    self._select(),self._from_ds(), self._group_by()
                    ))
