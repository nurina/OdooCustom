# -*- coding: utf-8 -*-

import time
from openerp import api, fields, models, _
from openerp.exceptions import UserError


class ReportGeneralLedger(models.AbstractModel):
    _name = 'report.lc_intercompany_report.intercompany_summary'
    
    def get_account_move_intercompany(self,analytic_id,accounts,initial_balance,date_from,date_to):
        cr = self.env.cr
        intercompany = '%Intercompany%'
        pusat_cab = '%pusat cabang%'
        bnk = '%bnk%'
        cash = '%csh%'
        rev = '%revisi%'
        migrasi = '%migrasi%'
        move_lines = dict(map(lambda x: (x, []), accounts.ids))
        account_ids = [account.id for account in accounts]
        
        

        
        # Prepare initial sql query and Get the initial move lines
        if initial_balance:
            sql = ("select True AS init,query.account_id as account_id,'' as date, '' as name,'' as v_label,'' as source_doc, '' as partner, 'INITIAL BALANCE' as ref, '' as type,'' as origin, sum(query.debit) as debit,sum(query.credit) as credit,sum(query.balance) as balance from \
             (SELECT m.analytic_account_id,m.date,m.account_id,COALESCE(SUM(m.debit),0.0) AS debit, COALESCE(SUM(m.credit),0.0) AS credit, COALESCE(SUM(m.debit),0) - COALESCE(SUM(m.credit), 0) as balance \
                from account_move_line m\
                left join account_move acc on acc.id = m.move_id\
                left join account_account a on a.id = m.account_id\
                left join stock_picking p on p.name = m.ref\
                left join account_journal j on j.id = m.journal_id\
                left join account_voucher v on v.move_id = acc.id\
                left join res_partner rp on m.partner_id = rp.id\
                 where (a.name ilike %s AND m.journal_id = '7') or (j.name ilike %s and a.name ilike %s and v.is_dropshipment_debit_credit_note is True) \
            or (j.name ilike %s and a.name ilike %s) or (a.name ilike %s and j.type = 'bank' or j.type = 'cash') or (a.name ilike %s and m.name ilike %s or m.name ilike %s) \
                 GROUP BY m.analytic_account_id,m.account_id,m.date) as query where\
                query.date < %s AND\
                query.analytic_account_id = %s AND\
                query.account_id in %s GROUP BY query.account_id")
            cr.execute(sql, (str(intercompany),str(pusat_cab),str(intercompany),str(pusat_cab),str(intercompany),str(intercompany),str(intercompany),str(rev),str(migrasi),date_from,analytic_id,tuple(account_ids)))
            for row in cr.dictfetchall():
                move_lines[row.pop('account_id')].append(row)
        
        
#         accounts = self.env['account.account'].search([])
#         accounts_res = self.env['report.account.report_generalledger'].with_context(data['form'].get('used_context',{}))._get_account_move_entry(accounts, self.initial_balance, self.sortby, self.display_account)
        query = '''select * from (SELECT False as init,m.analytic_account_id,m.account_id,acc.name as ref,j.code as code,acc.ref as source_doc,
                SUM(m.debit) as debit,
                SUM(m.credit) as credit,
                SUM(m.balance) as balance,
                             m.date as date,
                             rp.ref as partner,
                             m.partner_id as partner_id,
                             p.origin as origin,
                             v.is_dropshipment_debit_credit_note,
                             v.name as v_label,
                              case when j.code ilike %s
                                then 'TFLC'
                                when v.is_dropshipment_debit_credit_note = True
                                then 'Dropship'
                                else 'Others'
                                end as type
                from account_move_line m
                            left join account_move acc on acc.id = m.move_id
                            left join account_account a on a.id = m.account_id
                            left join stock_picking p on p.name = m.ref
                            left join account_journal j on j.id = m.journal_id
                            left join account_voucher v on v.move_id = acc.id
                            left join res_partner rp on m.partner_id = rp.id
                            where 
                             (j.name ilike %s and a.name ilike %s and v.is_dropshipment_debit_credit_note is True)
            or (j.name ilike %s and a.name ilike %s) or (a.name ilike %s and j.type = 'bank' or j.type = 'cash') or (a.name ilike %s and m.name ilike %s or m.name ilike %s)
                           
                           
                group by p.origin, m.ref, m.date, m.partner_id, j.code, v.is_dropshipment_debit_credit_note, rp.ref, m.analytic_account_id, m.account_id, v.name,acc.name,acc.ref
    
                UNION
                SELECT False as init,m.analytic_account_id,m.account_id,m.ref as ref,j.code as code,'' as source_doc,
                SUM(m.debit) as debit,
                SUM(m.credit) as credit,
                SUM(m.balance) as balance,
                             m.date as date,
                             rp.ref as partner,
                             m.partner_id as partner_id,
                             p.origin as origin,
                             v.is_dropshipment_debit_credit_note,
                             v.name as v_label,
                              case when j.code = 'STJ'
                                then 'BRQ' end as type
                from account_move_line m
                            left join account_move acc on acc.id = m.move_id
                            left join account_account a on a.id = m.account_id
                            left join stock_picking p on p.name = m.ref
                            left join account_journal j on j.id = m.journal_id
                            left join account_voucher v on v.move_id = acc.id
                            left join res_partner rp on m.partner_id = rp.id
                            where 
                            (a.name ilike %s AND m.journal_id = '7') 
                           
                           
                group by p.origin, m.ref, m.date, m.partner_id, j.code, v.is_dropshipment_debit_credit_note, rp.ref, m.analytic_account_id, m.account_id, v.name,acc.ref
               
                
                ) as query where
                query.date >= %s AND
                query.date <= %s AND
                query.analytic_account_id = %s AND
                query.account_id in %s
                order by query.date asc
                
                 '''
        cr.execute(query,(str(bnk),str(pusat_cab),str(intercompany),str(pusat_cab),str(intercompany),str(intercompany),str(intercompany),str(rev),str(migrasi),str(intercompany),date_from, date_to,analytic_id,tuple(account_ids)))
        
        for row in cr.dictfetchall():
            balance = 0
            for line in move_lines.get(row['account_id']):
                balance += line['debit'] - line['credit']
            if row['type'] == 'BRQ':
                row['origin'] = row['origin'].replace("Branch Request ", "") if row['origin'] and "Branch Request " in row['origin'] else row['origin']
                # else:
                #     continue
            if row['type'] == 'TFLC':
                row['origin'] = 'TFLC'
            row['balance'] += balance
            move_lines[row.pop('account_id')].append(row)
        # Calculate the debit, credit and balance for Accounts
        account_res = []
        accounts = self.env['account.account'].browse(account_ids)
        for account in accounts:
            res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
            res['code'] = account.code
            res['name'] = account.name
            res['move_lines'] = move_lines[account.id]
            for line in res.get('move_lines'):
                res['debit'] += line['debit']
                res['credit'] += line['credit']
                res['balance'] = line['balance']
            account_res.append(res)
        return account_res
    
    @api.multi
    def render_html(self, data):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
        init_balance = data['form'].get('initial_balance', True)
        analytic_id = data['form']['used_context'].get('analytic_id')
        account_ids = data['form']['account_ids']
        date_from = data['form']['used_context'].get('date_from')
        date_to = data['form']['used_context'].get('date_to')
        codes = []
        accounts = self.env['account.account'].browse(account_ids)
        accounts_res = self.with_context(data['form'].get('used_context',{})).get_account_move_intercompany(analytic_id,accounts,init_balance,date_from,date_to)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'Accounts': accounts_res,
            'print_journal': codes,
        }
        return self.env['report'].render('lc_intercompany_report.report_intercompanysummary', docargs)