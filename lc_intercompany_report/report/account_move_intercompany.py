# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import tools
from openerp import models, fields, api, _
from datetime import datetime,timedelta

class intercompany_report(models.Model):
    _name = "account.move.line.intercompany"
    _description = "Journal Item Intercompany report Pivot"
    _auto = False

    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    qty = fields.Float('Quantity', readonly=True)
    debit = fields.Float(string="Debit", readonly=True)
    credit = fields.Float(string="Credit", readonly=True)
    value = fields.Float(string="Balance", readonly=True)
    label = fields.Char('Label')
    account = fields.Many2one('account.account', 'Account')
    cost_price = fields.Float('Cost Price', group_operator = 'avg')
    analytic_account_id = fields.Many2one('account.analytic.account','Analytic Account')
    ref = fields.Char('Reference')
    date = fields.Date('Journal Date')
    journal_id = fields.Many2one('account.journal','Journal')
    type_report =fields.Selection([('brq','BRQ'),('ds','Dropship'), ('tflc','TFLC'), 
                                    ('no_ds','Non Dropship')],'Type Report')
    partner_id = fields.Many2one('res.partner', 'Partner')
    move_id = fields.Many2one('account.move', 'Journal Entry',readonly=True)
    branch_id = fields.Many2one('res.partner', "Branch")
    
    def _select(self):
        select_str = """
             SELECT  
             m.id as id,
             m.journal_id as journal_id, 
             m.name as label,
             m.product_id as product_id, 
             a.id as account, 
             sum(m.quantity) as qty , 
             m.related_price_unit as cost_price, 
             (m.debit - m.credit) as value,
             m.debit as debit,
             m.credit as credit,
             m.analytic_account_id as analytic_account_id, 
             m.ref as ref,
             m.date as date,
             m.partner_id as partner_id,
             acc.branch_id as branch_id,
             m.move_id as move_id
                          """ 
        return select_str

    def _from_brq(self):
        from_str = """
            account_move_line m
            left join account_move acc on acc.id = m.move_id
            left join account_account a on a.id = m.account_id
            left join stock_picking p on p.name = m.ref
            where 
            a.name ilike '%Intercompany%' AND
            m.journal_id = '7'
            """
        return from_str
    
#     def _from_bdn(self):
#         from_str = """
#             
#                 account_move_line m
#                 left join account_account a on a.id = m.account_id
#                 left join account_journal j on j.id = m.journal_id
#                 right join stock_picking p on p.name = m.ref
#                 WHERE 
#                 j.name ilike '%pusat cabang%' and a.name not ilike '%Intercompany%'
#                 
#             """
#         return from_str
#     
#     def _from_bcn(self):
#         from_str = """
#                 account_move_line m
#                 left join account_account a on a.id = m.account_id
#                 left join account_move acc on m.move_id = acc.id
#                 left join account_voucher v on v.move_id = acc.id
#                 left join account_journal j on j.id = m.journal_id
#                 right join ( 
#                     select  move.name as name
#                     from account_move move
#                         left join account_journal j on j.id = move.journal_id
#                         right join stock_picking p on p.name like move.ref
#                     where 
#                         j.name ilike '%pusat cabang%'
#                     group by move.name
#                     ) as k on k.name =  v.reference
#                 WHERE 
#                 j.name ilike '%pusat cabang%' and a.name not ilike '%Intercompany%'
#                 
#                 
#             """
#         return from_str
#     
    def _from_bdn_bcn(self):
        from_str = """
                account_move_line m
                left join account_account a on a.id = m.account_id
                left join account_journal j on j.id = m.journal_id
                left join account_move acc on acc.id = m.move_id
                left join account_voucher v on v.move_id = acc.id
                WHERE 
                j.name ilike '%pusat cabang%' and a.name ilike '%Intercompany%' and v.is_dropshipment_debit_credit_note is Null
                
            """
        return from_str
    
    def _from_ds(self):
        from_str = """
            account_move_line m
                left join account_account a on a.id = m.account_id
                left join account_journal j on j.id = m.journal_id
                left join account_move acc on acc.id = m.move_id
                left join account_voucher v on v.move_id = acc.id
                WHERE 
                j.name ilike '%pusat cabang%' and a.name ilike '%Intercompany%' and v.is_dropshipment_debit_credit_note is True
                
            """
        return from_str
    
    def _from_tflc(self):
        from_str = """
            account_move_line m
            left join account_move acc on acc.id = m.move_id
            left join account_account a on a.id = m.account_id
            WHERE 
            m.name ilike '%TFLC%' and a.name ilike '%Intercompany%'
            """
        return from_str

    def _group_by(self):
        group_by_str = """
            GROUP BY   
                m.id,
                m.journal_id , 
                 m.name,
                 m.product_id, 
                 a.id,  
                 m.related_price_unit , 
                 m.debit,
                 m.credit,
                 m.analytic_account_id , 
                 m.ref ,
                 m.date,
                 m.partner_id,
                 acc.branch_id,
                 m.move_id
        """
        return group_by_str

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute("""CREATE or REPLACE VIEW %s as (
            %s , %s 
                FROM  %s 
                %s, %s
            UNION
            %s , %s 
                FROM  %s 
                %s, %s
            UNION
            %s , %s 
                FROM  %s 
                %s, %s
            UNION
            %s , %s 
                FROM  %s 
                %s, %s
            )""" % (self._table, 
                    self._select(), """ 'tflc' as type_report """,self._from_tflc(), self._group_by(), """ type_report """,
                    self._select(), """ 'ds' as type_report """,self._from_ds(), self._group_by(), """ type_report """,
                    self._select(), """ 'brq' as type_report """,self._from_brq(), self._group_by(), """ type_report """,
                    self._select(), """ 'no_ds' as type_report """,self._from_bdn_bcn(), self._group_by(), """ type_report """
                    ))
