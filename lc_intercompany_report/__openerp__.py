{
    'name': 'Intercompany Pivot',
    'version': '1.0',
    'author': 'Port Cities',
    'description': """

v1.0
----
* present all data from intercompany account \n
-TFLC \n
-BRQ \n
-BCN / BDN
v2.0
----
* intercompany Summary


    """,
    'website': 'http://www.portcities.net',
    'depends': ['account','mrp'],
    'category': 'Laris accounting',
    'sequence': 16,
    'data': [
          'security/ir.model.access.csv',
          'wizard/intercompany_summary_view.xml',
           'views/account_report.xml',
         'report/account_move_intercompany.xml', 
         'report/report_debit_credit_dropship.xml', 
         'views/intercompany_summary_report.xml', 
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}