# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import models, fields, api, _
from openerp.exceptions import UserError
from openerp.tools.float_utils import float_compare

class account_payment(models.Model):
    _inherit = 'account.payment'
    
    @api.model
    def _allowed_invoice_access(self):
        domain = []
        if self.payment_type == 'inbound':
            domain = [('type', 'in', ('out_invoice'))]
        elif self.payment_type == 'outbound':
            domain = [('type', 'in', ('in_invoice'))]
        return domain
    
    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.allowed_invoice_ids:
            self.allowed_invoice_ids = False
            

    allowed_invoice_ids = fields.Many2many('account.invoice', string='Payment For', domain=_allowed_invoice_access,track_visibility='onchange')
    
    
    def message_post_field_m2m(self,name_field,new_field,old_field, obj):
        fields_ids = new_field[0]
        new_fields = []
        old_fields = []
        for temp_fields in fields_ids[2]:
            field_temp = self.env[obj].search([('id','=',temp_fields)])
            new_fields.append(field_temp.number)
        for field_id in old_field:
            old_fields.append(field_id.number)
        str_old_field = ",".join(str(x) for x in old_fields)
        str_new_field = ",".join(str(x) for x in new_fields)
        return self.message_post(body="%s %s -> %s"%(name_field,str_old_field,str_new_field))
    
    @api.multi
    def write(self, vals):
        
        if vals.get('allowed_invoice_ids'):
            assign_invoices = vals.get('allowed_invoice_ids')
            self.message_post_field_m2m('Payment For',assign_invoices,self.allowed_invoice_ids,'account.invoice')
        return super(account_payment, self).write(vals)
    
    