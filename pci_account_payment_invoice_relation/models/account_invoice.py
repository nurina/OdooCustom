# -*- coding: utf-8 -*-

import json
from openerp.osv import fields, osv
from openerp import models, fields, api, _
from openerp.exceptions import UserError
from openerp.tools.float_utils import float_compare
from openerp.tools import float_is_zero, float_compare
 
class account_invoice(models.Model):
    _inherit = 'account.invoice'
    
    @api.one
    def compute_payment_ids(self):
        payment_ids = self.env['account.payment'].search([
            ('partner_id','=',self.partner_id.id),
            ('state','=','draft'),
            ('allowed_invoice_ids', 'in', self.ids)]).ids
        self.allowed_payment_ids = [(6, 0, payment_ids)]
    
    @api.one
    def _compute_has_payment(self):
        is_filled_status = False
        if self.allowed_payment_ids:
            is_filled_status = True
        self.has_payment_ids = is_filled_status
        
    has_payment_ids = fields.Boolean(string='Has Payments', compute='_compute_has_payment', default=False)
    allowed_payment_ids = fields.Many2many('account.payment', string='Payments', compute='compute_payment_ids', readonly=True)
    
    
        
    