{
    "name": "PCI Payment Invoice Relation",
    "version": "1.0",
    "author": "Portcities Indonesia",
    "website": "http://portcities.net",
    "category": "Generic Modules",
    "depends": ["account"],
    "description": """
This module to provide allocation of registered payment to many invoice \n
Give information about draft giro / payment in invoice \n
*Nur Inayati


""",
    "demo_xml":[],
    "data":[
             'views/account_payment_view.xml',
            ],
    "active": False,
    "installable": True,
}