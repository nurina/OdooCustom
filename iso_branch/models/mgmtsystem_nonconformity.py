from openerp import models, fields, api, _
from openerp.exceptions import UserError

class ISONonconformities(models.Model):
    _inherit = "mgmtsystem.nonconformity"
    
    branch_id = fields.Many2one('res.partner', "Branch")
    department_id = fields.Many2one('hr.department', "Department")
    partner_id = fields.Many2one('res.partner', 'Partner')
    
    _defaults = {
        'analysis': """1. \n2. \n3. \n4. \n"""
    }
    
    @api.constrains('stage_id')
    def _check_open_with_action_comments(self):
        for nc in self:
            action_ids = nc.action_ids.filtered(lambda r: r.stage_id.name != 'Closed')
            if nc.state == 'open' and action_ids:
                raise models.ValidationError(
                    _("All Action plan  must be closed "
                      "in order to put a nonconformity to Ready to Close."))
    @api.model
    def create(self, vals):
        res = super(ISONonconformities, self).create(vals)
        sequence_id = self.env['ir.sequence'].search([
                ('code', '=', 'mgmtsystem.nonconformities.sequence.code'),
                ('branch_id', '=', res.branch_id.id)])
        sequence = sequence_id.next_by_id()
        res.ref = 'FTP/%s/%s'%(res.department_id.short_code, sequence)
        return res
    
    