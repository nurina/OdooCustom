import logging
from openerp import models, fields, api, _

_logger = logging.getLogger(__name__)
class ISOPageHistory(models.Model):
    _inherit = "document.page"
    
    @api.multi
    def _get_version_number(self):
        for page in self:
            for index,history in zip(range(1),page.history_ids.filtered(lambda r: r.state == 'approved')):
                page.version_number = history.version_number
    doc_number = fields.Char('Document Number')
    department_id = fields.Many2one('hr.department', "Department")
    version_number = fields.Char(string='Version Number', size=2,compute=_get_version_number)
    created_uid = fields.Many2one(
        'res.users',
        string="Created By",
    )
    checked_uid = fields.Many2one(
        'res.users',
        string="Checked By",
    )
    
    
class DocumentPageHistoryWorkflow(models.Model):
    """Useful to manage edition's workflow on a document."""
    
    _inherit = 'document.page.history'
    
    
    @api.multi
    def page_approval_draft(self):
        """Set a document state as draft and notified the reviewers."""
        self.write({'state': 'draft'})
        return True
    
    @api.multi
    def page_approval_branch_head(self):
        """Set a document state as draft and notified the reviewers."""
        template = self.env.ref(
            'iso_branch.email_template_head_office_approval')
        for page in self:
            if page.is_parent_approval_required:
                template.send_mail(page.id, force_send=True)
        return True
    
    
    @api.multi
    def page_send_related_users(self):
        """Set a document state as draft and notified the reviewers."""
        template = self.env.ref(
            'iso_branch.email_template_broadcast_users')
        for page in self:
            if page.is_parent_approval_required:
                template.send_mail(page.id, force_send=True)
        return True
    
    
    @api.multi
    def _get_approvers_head_email(self):
        """Get the approvers email."""
        for page in self:
            group_head_user = self.env['ir.model.data'].xmlid_to_res_id('document_page_approval.group_document_approver_user')
            
            self._cr.execute("select rp.email, rp.id, rp.name from res_groups_users_rel rel "\
                             "inner join res_users ru on rel.uid=ru.id "\
                             "inner join res_partner rp on rp.id=ru.partner_id "\
                             "where rel.gid in (%s) and ru.active = True " %(group_head_user))
            recipient_ids = []
            # Avoid duplicate user on TO
            for res in self._cr.fetchall():
                if res[0] not in recipient_ids:
                    recipient_ids.append(res[0])

            str_recipient_ids = ",".join(str(x) for x in recipient_ids)
            page.get_approvers_head_email = str_recipient_ids
    
    
    @api.multi
    def _get_broadcast_related_email(self):
        """Get the approvers email."""
        for page in self:
            message_author_ids = page.message_ids.mapped('author_id')
            recipient_ids = [message_author.email for message_author in message_author_ids]
            employees = self.env['hr.employee'].search([('department_id','=',page.page_id.department_id.id)])
            if employees:
                for employee in employees:
                    recipient_ids.append(employee.work_email)
            set = {}
            map(set.__setitem__, recipient_ids, [])
            final_recipient = set.keys()
            str_recipient_ids = ",".join(str(x) for x in final_recipient)
            page.get_broadcast_related_email = str_recipient_ids
     
    @api.multi
    def _get_approvers_qmr_email(self):
        """Get the approvers email."""
        for page in self:
            group_qmr_user = self.env['ir.model.data'].xmlid_to_res_id('iso_branch.group_document_qmr_approver_user')
            
            self._cr.execute("select rp.email, rp.id, rp.name from res_groups_users_rel rel "\
                             "inner join res_users ru on rel.uid=ru.id "\
                             "inner join res_partner rp on rp.id=ru.partner_id "\
                             "where rel.gid in (%s) and ru.active = True " %(group_qmr_user))
            recipient_ids = []
            # Avoid duplicate user on TO
            for res in self._cr.fetchall():
                if res[0] not in recipient_ids:
                    recipient_ids.append(res[0])

            str_recipient_ids = ",".join(str(x) for x in recipient_ids)
            page.get_approvers_qmr_email = str_recipient_ids
    
    state = fields.Selection(
        [('draft', 'Draft'), ('ready_to_approve', 'Ready to Approve'),('approved_by_qmr', 'Approved By QMR'),('approved', 'Approved')],
        'Status',
        readonly=True
    )
    
    get_approvers_qmr_email = fields.Text(
        compute=_get_approvers_qmr_email,
        string="get all approvers email",
        store=False
    )
    
    get_broadcast_related_email = fields.Text(
        compute=_get_broadcast_related_email,
        string="get related user email",
        store=False
    )

    get_approvers_head_email = fields.Text(
        compute=_get_approvers_head_email,
        string="get all approvers email",
        store=False
    )
    
    version_number = fields.Char(string='Version Number', size=2)
    
    @api.model
    def create(self,vals):
        res = super(DocumentPageHistoryWorkflow, self).create(vals)
        if not vals.get('version_number'):
            history_length = len(res.page_id.history_ids)
            if history_length > 1:
                page_obj = self.env['document.page.history'].search([('page_id','=',res.page_id.id)])
                number = int(page_obj[1].version_number)
                version = number + 1
                char_version = "0%s"%(version)
                res.version_number= char_version[-2:]
            else:
                res.version_number= '01'
        return res
    
    @api.multi
    def page_approval_approved(self):
        self.sudo().page_send_related_users()
        return super(DocumentPageHistoryWorkflow, self).page_approval_approved()
    
    @api.multi
    def page_approval_qmr(self):
        """Set a document state as approve."""
        message_obj = self.env['mail.message']
        self.write({
            'state': 'approved_by_qmr',
        })
        # Notify followers a new version is available
        for page_history in self:
            subtype = self.env.ref('mail.mt_comment')
            page_history.page_id.write({
            'checked_uid': self.env.user.id,
        })
            note = 'New version %s %s of the document had been approved By %s.'%(page_history.page_id.name,page_history.version_number,self.env.user.name)
            page_history.message_post(
                                  body=note,
                                  subject="Note")
            
            message_obj.create(
                {'res_id': page_history.page_id.id,
                 'model': 'document.page',
                 'subtype_id': subtype.id,
                 'body': note
                 }
            )
    
            
        self.sudo().page_approval_branch_head()
        return True