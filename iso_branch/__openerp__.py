# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'ISO Branch',
    'version': "1.00",
    'author': "PortCities",
    'category': "ISO Branch",
    'description': """
        Author: Nur Inayati \n
        - Give branch and department nonconformities \n
        - add document number on Pages (knowledge) \n
        - Give Default on analysis field on nonconformities \n
        - Make partner_id not required on nonconformities
        - Hide fields (related to, causes, analysis confirmation)
        """,
    'data': [
             "wizard/confirmation_approve_wizard.xml",
            "views/mgmtsystem_views.xml",
            "security/document_page_security.xml",
            "security/ir.model.access.csv",
            "data/email_template_head_approve.xml",
            "data/email_template_user_broadcast.xml",
            "data/email_template_draft.xml",
            "data/ir_sequence_nonconformities_data.xml",
            "report/knowledge_page_report.xml",
            "report/report.xml",
            
        
    ],
    'depends': ['mgmtsystem',"mgmtsystem_nonconformity"],
    'auto_install': False,
    'installable': True,
    'application': True,
}
