from openerp import models, fields,api, _
from openerp import workflow
from openerp.exceptions import UserError, ValidationError
from openerp import exceptions

class cancel_confirmation_wizard(models.TransientModel):
    _name = "knowledge.ready.approve.wizard"
    
    note = fields.Text('Details Note', default="Detail Pembuatan\n \n \n \nAlasan Pembuatan\n \n \n \n")
    active_id = fields.Integer('active id')
    
    
    @api.multi
    def action_ready_approve(self):
        if len(self.note) > 120:
            obj = self.env['document.page.history'].browse(self.active_id)
            obj.write({'state':'ready_to_approve'})
            obj.page_id.created_uid = self.env.user.id
            template = self.env.ref(
                'iso_branch.new_email_template_new_draft_need_approval')
            ctx = {
                  'notes': self.note,
                }
            for page in obj:
                if page.is_parent_approval_required:
                    template.with_context(ctx).sudo().send_mail(obj.id, force_send=True)
            obj.message_post(
                                  body=self.note,
                                  subject="Note")
            obj.page_id.message_post(
                                  body=self.note,
                                  subject="Note")
        else:
            raise exceptions.Warning(
                _("Sorry! You cannot confirm because notes less than 120 character")
            )
        return True