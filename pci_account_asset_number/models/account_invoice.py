from openerp import api, fields, models
from openerp.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_cancel(self):
        res = super(AccountInvoice, self).action_cancel()
        asset_ids = self.env['account.asset.asset'].sudo().search([('invoice_id', 'in', self.ids)])
        if asset_ids:
            if asset_ids.mapped('account_move_ids'):
                raise UserError('You cannot cancel this vendor bill, because asset have been depreciated. Please check related asset first!')

            # for asset_id in asset_ids.filtered(lambda a: a.invoice_line_id):
            #    asset_id.invoice_line_id.asset_code_related = asset_id.asset_code

            # for asset_id in asset_ids.filtered(lambda a: not a.invoice_line_id):
            #    invoice_line_ids = self.invoice_line_ids.filtered(
            #        lambda line: line.name == asset_id.name)
            #    invoice_line_ids.asset_code_related = asset_id.asset_code

        return res

    @api.multi
    def action_move_create(self):
        result = super(AccountInvoice, self).action_move_create()
        asset_ids = self.env['account.asset.asset'].sudo().search([
            ('invoice_id', 'in', self.ids), ('active', '=', False)])
        if asset_ids:
            for asset_id in asset_ids:
                asset_id.set_to_draft()
                asset_id.unlink()
        return result


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    # asset_code_related = fields.Char('Old Asset Code Related', default="/",
    #     help="This field is use to keep the old sequences of the related asset that was attached before")

    @api.one
    def asset_create_with_asset_code(self):
        if self.asset_category_id:
            vals = {
                'name': self.name,
                'code': self.invoice_id.number or False,
                'category_id': self.asset_category_id.id,
                'value': self.price_subtotal_signed,
                'partner_id': self.invoice_id.partner_id.id,
                'company_id': self.invoice_id.company_id.id,
                'currency_id': self.invoice_id.company_currency_id.id,
                'date': self.invoice_id.date_invoice,
                'invoice_id': self.invoice_id.id,
                'invoice_line_id': self.id,
                # 'asset_code': self.asset_code_related,
            }
            changed_vals = self.env['account.asset.asset'].onchange_category_id_values(vals['category_id'])
            vals.update(changed_vals['value'])
            asset = self.env['account.asset.asset'].create(vals)
            if self.asset_category_id.open_asset:
                asset.validate()
        return True

    @api.one
    def asset_create(self):
        if self.invoice_id.date_invoice or self.asset_code_related:
            return self.asset_create_with_asset_code()
        else:
            return super(AccountInvoiceLine, self).asset_create()
