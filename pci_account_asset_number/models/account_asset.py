from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
from openerp.tools.translate import _


class AccountAsset(models.Model):
    _inherit = 'account.asset.asset'


    # asset_code = fields.Char(string='Asset Number', default="/",
    #                         help="This field is shown the asset number")
    invoice_line_id = fields.Many2one('account.invoice.line', string="Bill line", help="This field is use to keep bill line as per asset that created based on bill lines")

    # @api.model
    # def create(self, values):
    #    asset_id = False
    #    if values.get('asset_code') and values.get('asset_code') != "/":
    #        asset_id = self.search([('active', '=', True),
    #                                ('asset_code', 'ilike', values.get('asset_code'))], limit=1)
    #
    #    if not asset_id:
    #        return super(AccountAsset, self).create(values)
    #    else:
    #        raise ValidationError(_('Asset number is already exist!'))

    # @api.multi
    # @api.depends('name', 'asset_code')
    # def name_get(self):
    #    res = []
    #    for record in self:
    #        name = record.name
    #        if record.asset_code:
    #            name = '[%s] - %s' %(record.asset_code, name)
    #        res.append((record.id, name))
    #    return res

    # @api.multi
    # def validate(self):
    #    res = super(AccountAsset, self).validate()
    #    for asset_id in self.filtered(lambda a: a.asset_code == "/"):
    #        asset_id.assigned_asset_number()
    #    return res

    @api.multi
    def assigned_asset_number(self):
        for asset_id in self:
            sequence_id = self.env['ir.sequence'].search([
                ('code', '=', 'account.asset.sequence.code'),
                ('branch_id', '=', asset_id.branch_id.id)])

            if not sequence_id:
                continue

            asset_id.asset_code = sequence_id.with_context(
                ir_sequence_date=asset_id.date).next_by_id()
#             references = '%s - %s' %(asset_id.invoice_id.number, asset_id.asset_code)
#             asset_id.code = references if asset_id.code else asset_id.code
