# coding: utf-8
""" Libraries Required """
from openerp import models, api


class AccountAssetBatchNumber(models.TransientModel):
    """
    Account move batch number wizard, it would generate asset code / sequences as per batch.
    """
    _name = 'account.asset.batch.number'
    _description = 'Account asset batch number'

    @api.multi
    def action_generate_batch_asset_number(self):
        """
            All selected asset will be received new number / sequences
        """
        active_ids = self._context.get('active_ids', False)
        asset_ids = self.env['account.asset.asset'].browse(active_ids)
        asset_ids.assigned_asset_number()
