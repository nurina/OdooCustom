# -*- coding: utf-8 -*-
{
    'name': 'PCI Account Asset Numer',
    'version': '1.0',
    'author': 'PCI',
    'description': """

v1.0\n
----\n
\n
[ADD] Add asset code to asset \n
(case)
-auto increment on asset code \n
-use old number asset when vedor bill is canceled \n
-editable asset on vendor bill line to avoid gap number (assign manual) \n
--- \n
v2.0 \n
--- \n
[IMP] Update Delete asset when revalidate vendor bill \n
Nur Inayati
v3.0 \n
--- \n
[ADD] Added action multi to generate & assigned number on Batch Assets

""",
    'depends': ['account', 'account_asset', "lc_sequence_branches"],
    'data': [
        # "data/ir_sequence_asset_data.xml",
        # "views/account_asset_view.xml",
        # "views/account_invoice_view.xml",
        # "wizards/account_asset_batch_number_view.xml"
    ],
    'installable'   : True,
    'application'   : False,
    'auto_install'  : False,
}
