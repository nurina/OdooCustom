# -*- coding: utf-8 -*-

from collections import defaultdict
from openerp.osv import fields, osv
from openerp import SUPERUSER_ID,models, fields, api, _
from openerp.exceptions import UserError
from openerp.tools import float_compare, float_is_zero


class StockQuant(models.Model):
    _inherit = 'stock.quant'
    
    def _get_inventory_value(self, cr, uid, quant, context=None):
        res = super(StockQuant, self)._get_inventory_value(cr, uid, quant, context=context)
        if quant.product_id.cost_method in ('average'):
            return quant.cost * quant.qty
        return res

class StockMove(models.Model):
    _inherit = 'stock.move'
    
    
    @api.multi
    def product_price_update_before_done(self):
        tmpl_dict = defaultdict(lambda: 0.0)
        # adapt standard price on incomming moves if the product cost_method is 'average'
        std_price_update = {}
        for move in self.filtered(lambda move: move.location_id.usage in ('supplier', 'production') and move.product_id.cost_method == 'average'):
            product_tot_qty_available = move.product_id.qty_available + tmpl_dict[move.product_id.id]
            price_unit = self.env['stock.move'].get_price_unit(move)

            # if the incoming move is for a purchase order with foreign currency, need to call this to get the same value that the quant will use.
            if product_tot_qty_available <= 0:
        
                new_std_price = price_unit
            else:
                # Get the standard price
                amount_unit = std_price_update.get((move.company_id.id, move.product_id.id)) or move.product_id.standard_price
                new_std_price = ((amount_unit * product_tot_qty_available) + (price_unit * move.product_qty)) / (product_tot_qty_available + move.product_qty)
            tmpl_dict[move.product_id.id] += move.product_qty
            # Write the standard price, as SUPERUSER_ID because a warehouse manager may not have the right to write on products
            move.product_id.with_context(force_company=move.company_id.id).sudo().write({'standard_price': new_std_price})
            std_price_update[move.company_id.id, move.product_id.id] = new_std_price
                
        
    