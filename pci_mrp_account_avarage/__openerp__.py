# -*- coding: utf-8 -*-
{
    "name": "PCI MRP Account Average",
    "version": "1.0",
    "author": "Portcities Indonesia",
    "website": "http://portcities.net",
    "category": "Generic Modules",
    "depends": ["mrp","stock_account"],
    "description": """
Fix calculation of journal persediaan barang jadi \n
Default odoo : \n
If cost method is average. Cost price will take from cost price's product ,\n

What we want :\n
It will take valuation amount from price unit of componets


""",
    "demo_xml":[],
    "data":[],
    "active": False,
    "installable": True,
}