from openerp import models, fields, api, _
from openerp.exceptions import UserError

class HrJob(models.Model):
    _inherit = "hr.job"
    
    @api.model
    def _get_job_desc_default(self):
        default_desc = """ <h1>Tujuan Jabatan</h1>

                                <br/><br/><br/>
                                
                                <h1>Tugas dan Tanggung Jawab</h1>
                                 
                                 1. <br/> <br/>
                                 2. <br/> <br/>
                                 3. <br/> <br/>
                                 4. <br/> <br/>
                                 5. <br/> <br/>
                                 6. <br/> <br/>
                                 7. <br/> <br/>
                                 8. <br/> <br/>
                                 9. <br/> <br/>
                                
                                <h1>Wewenang</h1>
                                
                                 1. <br/> <br/>
                                 2. <br/> <br/>
                                 3. <br/> <br/>
                                
                                <h1>Hubungan Kedinasan</h1>
                                Pihak Internal : <br/><br/>
                                
                                 1. <br/> <br/>
                                 2. <br/> <br/>
                                 3. <br/> <br/>
                                 4. <br/> <br/>
                                 5. <br/> <br/>
                                
                                Pihak Eksternal : <br/><br/>
                                 1. <br/> <br/>
                                 2. <br/> <br/>
                                 3. <br/> <br/>
                                 
                                
                                Kompetensi Jabatan
                                Pendidikan :  <br/> <br/>
                                
                                Pengalaman Kerja : <br/> <br/>
                                
                                Pelatihan : <br/><br/>
                                
                                 1. <br/> <br/>
                                 2. <br/> <br/>
                                 3. <br/> <br/>
                                
                                Keterampilan dan Keahlian : <br/><br/>
                                 1. <br/> <br/>
                                 2. <br/> <br/>
                                 3. <br/> <br/>
                                 4. <br/> <br/>
                                  """
        return default_desc
    
    branch_id = fields.Many2one('res.partner', "Branch")
    
    _defaults = {'description':_get_job_desc_default
                 }
    


class HrEmployee(models.Model):
    _inherit = "hr.employee"
    
    branch_id = fields.Many2one('res.partner', "Branch")
    employee_code = fields.Char('Employee Code')
    nickname = fields.Char('Nickname')
    work_status = fields.Selection([('contract1', 'Contract 1'), ('contract2', 'Contract 2'),('probation', 'Probation'),('tetap','Tetap')],
        'Work Status')
    join_date = fields.Date(string='Join Date',
        help="Join Date")
    npwp = fields.Char('NPWP (Nomor Pokok Wajib Pajak)')
    emergency_name = fields.Char('Name')
    emergency_relation = fields.Char('Family Relationship')
    emergency_phone = fields.Char('Phone Number')

class HrDepartment(models.Model):
    _inherit = "hr.department"
    
    short_code = fields.Char('Short Code')