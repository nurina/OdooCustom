# -*- coding: utf-8 -*-
{
    'name': 'Cross Check Data Before to Confirm as Produce',
    'version': '11.0.0.1.0',
    'author': 'Port Cities',
    'category': '',
    'description': """
        Author:\n
        PC\n
        * Create wizard Manufacturing Order’s Validation Form
        """,
    'depends': ['mrp'],
    'data' : [
        'views/mrp_view.xml',
        'wizard/mrp_cross_check_wiz_view.xml',
    ],
    'qweb': [],
    'active': False,
    'installable': True,
    'application' : False,
    'auto_install' : False,
}
