# pylint: disable=C0301
# coding: utf-8
""" docstring pk_mrp """
from openerp import models, fields, api, tools, _
from openerp.exceptions import UserError


class MrpCrossCheckWiz(models.TransientModel):
    """
    Wizard form view of pos inventory
    """
    _name = 'mrp.cross.check.wiz'

    product_qty = fields.Float(string='Product Quantity')
    image = fields.Binary(string='image', attachment=True, help="Image")
    product_id = fields.Many2one('product.product', string='Product')

    @api.model
    def default_get(self, fields):
        res = super(MrpCrossCheckWiz, self).default_get(fields)
        mrp = self.env['mrp.production'].browse(self._context.get('active_id'))
        res.update({
            'product_id': mrp.product_id.id,
            'image': mrp.product_id.image_medium
        })
        return res

    @api.multi
    def action_confirm(self):
        """
	    Action Confirm to Produce after the validation is passed
	    """
        notification = self._action_validation()

        if notification.get('has_exception'):
            raise UserError(_(notification.get('content_message')))
        else:
            active_id = self.env.context.get('active_id')
            self.env['mrp.production'].browse(active_id).signal_workflow('button_confirm')

    @api.multi
    def _action_validation(self):
        notification = dict()
        content_message = """ Sorry! you're cannot Confirm Production.
        Because the data is different between MO form and this Form.
        \nBelow are the details information:\n
        """
        obj_mrp = self.env['mrp.production']
        mrp_id = self.env.context.get('active_id')
        mrp = obj_mrp.search([('id', '=', mrp_id)])

        if not self.product_qty == mrp.product_qty:
            content_message += """- Product Quantity for Product %s not the same as with Product Quantity in Manufacturing Order (MO).
            """ %(mrp.product_id.name)
            notification.update({'has_exception': True,})

        notification.update({
            'content_message': content_message,
        })

        content_message += "\n\n-- Please cross check with your notes, Thanks You --"
        notification.update({'content_message': content_message})

        return notification
