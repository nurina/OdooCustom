# coding: utf-8
""" docstring pk_mrp """
from openerp import models, api


class MrpProduction(models.Model):
    """
        Manufacturing Production ORder
    """
    _inherit = "mrp.production"

    @api.multi
    def action_validation_open(self):
        """
        function call wizard
        """
        view_id = self.env.ref('pci_mrp_crosscheck_data.mrp_cross_check_wiz_view_form').id
        return {
            'name': 'Manufacturing Order’s Validation Form',
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view_id, 'form')],
            'res_model': 'mrp.cross.check.wiz',
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
