# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api, _
from datetime import datetime, timedelta


class confirm_box(models.TransientModel):
    _name = 'account.depreciation.asset.wizard'
    
    depreciate_date = fields.Date('Depreciation Date', default=fields.Date.today())
    
    @api.multi
    def confirm_depreciate_all(self):
        self.ensure_one()
        asset_id = self.env.context.get('active_id')
        account_asset = self.env['account.asset.asset'].browse(asset_id)
        for depreciation_line in account_asset.depreciation_line_ids.filtered(lambda line:line.depreciation_date <= self.depreciate_date):
            depreciation_line.create_move()
        return True
    
    