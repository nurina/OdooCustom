# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api, _


class confirm_box(models.TransientModel):
    _name = 'account.asset.wizard'
    
    sale_or_dispose_date = fields.Date('Sale or Dispose Date', default=fields.Date.today)
    
    @api.multi
    def confirm_sales_dispose(self):
        self.ensure_one
        asset_id = self.env.context.get('active_id')
        account_asset = self.env['account.asset.asset'].browse(asset_id)
        account_asset.action_sales_dispose(self.sale_or_dispose_date)
        return True
    
    