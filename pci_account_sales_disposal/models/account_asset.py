# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import models, fields, api, _
from openerp.exceptions import UserError
from openerp.tools.float_utils import float_compare

class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'
    
    profit_or_loss_account = fields.Many2one('account.account', string='Profit or Loss (Sales or Disposal) Asset')
    fixed_asset_account = fields.Many2one('account.account', string='Fixed Asset Account')


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'
    
    state = fields.Selection([('draft', 'Draft'), ('open', 'Running'), ('depreciated', 'Depreciated'), ('close', 'Close')], 'Status', required=True, copy=False, default='draft',
        help="When an asset is created, the status is 'Draft'.\n"
            "If the asset is confirmed, the status goes in 'Running' and the depreciation lines can be posted in the accounting.\n"
            "You can manually close an asset when the depreciation is over. If the last line of depreciation is posted, the asset automatically goes in that status.")
    sale_or_dispose_state = fields.Boolean(string='Closed Status', track_visibility='always', default=False)
    
    @api.multi
    def action_sales_dispose(self,date_asset,post_move=True):
        created_moves = self.env['account.move']
        move_vals = {}
        for asset in self:
            closed_date = date_asset
            company_currency = asset.company_id.currency_id
            current_currency = asset.currency_id
            asset_name = 'Closed %s'%(asset.name)
            fix_asset_acc = asset.category_id.fixed_asset_account.id
            depre_acc = asset.category_id.account_depreciation_id.id
            profit_or_loss_acc = asset.category_id.profit_or_loss_account.id
            sign = (asset.category_id.journal_id.type == 'purchase' or asset.category_id.journal_id.type == 'sale' and 1) or -1
            gross_value = asset.value
            residual_value = asset.value_residual
            reference = asset.code
            journal_id = asset.category_id.journal_id.id
            partner_id = asset.partner_id.id
            categ_type = asset.category_id.type
            if residual_value == 0 or gross_value == residual_value:
                move_line_1 = {
                    'name': asset_name,
                    'account_id': fix_asset_acc,
                    'credit': gross_value,
                    'journal_id': journal_id,
                    'partner_id': partner_id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and - sign * gross_value or 0.0,
                    'analytic_account_id': asset.category_id.account_analytic_id.id if categ_type == 'sale' else False,
                    'date': closed_date,
                }
                
                move_line_2 = {
                    'name': asset_name,
                    'account_id': profit_or_loss_acc,
                    'debit': gross_value,
                    'journal_id': journal_id,
                    'partner_id': partner_id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and - sign * gross_value or 0.0,
                    'analytic_account_id': asset.category_id.account_analytic_id.id if categ_type == 'sale' else False,
                    'date': closed_date,
                }
                move_vals = {
                'ref': reference,
                'date': closed_date or False,
                'journal_id': asset.category_id.journal_id.id,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
                'asset_id': asset.id,
                }
            elif residual_value > 0:
                diff_value = gross_value - residual_value

                move_line_1 = {
                    'name': asset_name,
                    'account_id': fix_asset_acc,
                    'credit': gross_value,
                    'journal_id': journal_id,
                    'partner_id': partner_id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and - sign * gross_value or 0.0,
                    'analytic_account_id': asset.category_id.account_analytic_id.id if categ_type == 'sale' else False,
                    'date': closed_date,
                }
                
                move_line_2 = {
                    'name': asset_name,
                    'account_id': profit_or_loss_acc,
                    'debit': residual_value,
                    'journal_id': journal_id,
                    'partner_id': partner_id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and - sign * gross_value or 0.0,
                    'analytic_account_id': asset.category_id.account_analytic_id.id if categ_type == 'sale' else False,
                    'date': closed_date,
                }
                
                move_line_3 = {
                    'name': asset_name,
                    'account_id': depre_acc,
                    'debit': diff_value,
                    'journal_id': journal_id,
                    'partner_id': partner_id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and - sign * gross_value or 0.0,
                    'analytic_account_id': asset.category_id.account_analytic_id.id if categ_type == 'sale' else False,
                    'date': closed_date,
                }
                move_vals = {
                'ref': reference,
                'date': closed_date or False,
                'journal_id': asset.category_id.journal_id.id,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2),(0, 0, move_line_3)],
                'asset_id': asset.id,
                }
            asset.write({'sale_or_dispose_state': True})   
            move = self.env['account.move'].create(move_vals)
            asset.write({'state': 'close'})
            created_moves |= move

        if post_move and created_moves:
            created_moves.filtered(lambda r: r.asset_id and r.asset_id.category_id and r.asset_id.category_id.open_asset).post()
        return [x.id for x in created_moves]


class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'
    
    @api.multi
    def post_lines_and_close_asset(self):
        super(AccountAssetDepreciationLine, self).post_lines_and_close_asset()
        for line in self:
            asset = line.asset_id
            if asset.currency_id.is_zero(asset.value_residual) and asset.sale_or_dispose_state == False:
                asset.write({'state': 'depreciated'})
        
        
        
    