{
    "name": "PCI Account Sales Disposal",
    "version": "1.0",
    "author": "Portcities Indonesia",
    "website": "http://portcities.net",
    "category": "Generic Modules",
    "depends": ["account_asset"],
    "description": """
revise generated journals on sales or disposal button \n
*Nur Inayati


""",
    "demo_xml":[],
    "data":[
        'wizard/account_asset_wiz.xml',
        'wizard/asset_line_depre_wiz.xml',
        'views/asset_view.xml',
    ],
    "active": False,
    "installable": True,
}