# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from datetime import datetime,date,timedelta,date
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_log = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit ="res.partner"

    def display_as_monetary(self, amount):
        return '{0:,.2f}'.format(amount)
    @api.multi
    def display_payments(self, invoice):
        payments = []
        if invoice.allowed_payment_ids:
            for payment in invoice.allowed_payment_ids:
                payments.append(payment)
        return payments 
    
    def date_format_2digit(self, value):
        return fields.Date.from_string(value).strftime('%d/%m/%y')
    
    def date_format(self, value):
        return fields.Date.from_string(value).strftime('%d/%m/%Y')
    
    @api.model
    def action_warning_to_overdate_invoices(self):
        template_id = self.env.ref('pci_branch_invoice_overdate_notification.template_invoice_overdate')
        # Active Single Company
        company_id = self.env.ref('base.main_company')

        branch_invoices = [{
                'branch_id': partner,
                'invoices': [invoice for invoice in self.env['account.invoice'].get_invoice_overdate(
                    company_id.maximum_invoice_over_date) if invoice.branch_id.id == partner.id],
            } for partner in self.search([('branch', '=', True)])
        ]
        
        for partner in branch_invoices:
            # GET BY ACCOUNT ID
            account_id_grouped_set = set(map(lambda r:r['account_id'], partner['invoices']))
            # GROUPING BY ACCOUNT ID
            invoices = [{
                'account_id': account_id,
                'invoice_ids': [invoice for invoice in partner['invoices'] if invoice.account_id.id == account_id.id]
              } for account_id in account_id_grouped_set
            ]

            group_head_id = self.env['ir.model.data'].xmlid_to_res_id('branch_sale.branch_head_sales')
            group_manager_id = self.env['ir.model.data'].xmlid_to_res_id('base.group_sale_manager')
            group_account_head_id = self.env['ir.model.data'].xmlid_to_res_id('branch_accounting.branch_head_account')
            
            self._cr.execute("select rp.email, rp.id, rp.name from res_groups_users_rel rel "\
                             "inner join res_users ru on rel.uid=ru.id "\
                             "inner join res_partner rp on rp.id=ru.partner_id "\
                             "where rel.gid in (%s, %s, %s) and ru.id in (select user_id from branch_notifications where branch_id = %s) and ru.active = True " %(group_head_id, group_manager_id, group_account_head_id,partner['branch_id'].id))
            recipient_ids = []
            # Avoid duplicate user on TO
            for res in self._cr.fetchall():
                if res[0] not in recipient_ids:
                    recipient_ids.append(res[0])

            str_recipient_ids = ",".join(str(x) for x in recipient_ids)

            # Only main branch pluit who get cc email
            self._cr.execute("select rp.email from res_groups_users_rel rel "\
                             "inner join res_users ru on rel.uid=ru.id "\
                             "inner join res_partner rp on rp.id=ru.partner_id "\
                             "where rp.branch_id = 7 and rel.gid in (%s,%s) and ru.active = True" %(group_head_id, group_account_head_id))
            
            cc_ids = []

            # All branch get cc except pluit
            if partner['branch_id'].id != 7:
                for res in self._cr.fetchall():
                        if res[0] not in cc_ids:
                            cc_ids.append(res[0])
            str_cc_ids = ",".join(str(x) for x in cc_ids)

            ctx = {
              'recipient_ids': str_recipient_ids,
              'email_cc': str_cc_ids,
              'invoices': invoices,
              'maximum_date_in_days': company_id.maximum_invoice_over_date,
            }
            # SENDING EMAIL
            template_id.with_context(ctx).send_mail(partner['branch_id'].id, force_send=True)
    
        return True
            
