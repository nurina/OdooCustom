# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from datetime import datetime,date,timedelta,date
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_log = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit ="account.invoice"

    @api.multi
    def get_invoice_overdate(self, maximum_date_in_days):
        invoices = []
        for invoice in self.search([('state', '=', 'open'), ('type','in',('out_invoice', 'out_refund'))]):
            date_invoice = datetime.now().date() - datetime.strptime(
                  invoice.date_invoice, DEFAULT_SERVER_DATE_FORMAT).date()        
            if date_invoice.days  > maximum_date_in_days:
                invoices.append(invoice)
        return invoices