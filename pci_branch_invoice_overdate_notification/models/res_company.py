# -*- coding: utf-8 -*-
from openerp import models, fields


class ResCompany(models.Model):
    _inherit ="res.company"
     
    maximum_invoice_over_date = fields.Integer(
        string="Maximum Over Date of Invoice in Days"
    )
    