# -*- coding: utf-8 -*-
{
    'name'          : 'PCI Branch Invoice Overdate Notification',
    'version'       : '1.0',
    'author'        : 'PCI',
    'description'   : """

v1.0
----
Able to collect the invoices are over date more than specific days group by branches and account receivable in Customer
Able to sent the list invoices over email per weeks or days or month.




                      """,
    'depends'       : ['base_indonesia'],
    'data'          : [
       'data/email_notification.xml',
       'data/ir_cron.xml',
       'views/res_company_view.xml',                   
    ],
    'installable'   : True,
    'application'   : True,
    'auto_install'  : False
}