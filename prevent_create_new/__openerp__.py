# -*- coding: utf-8 -*-
{
    'name'          : 'Prevent Create New',
    'version'       : '1.0',
    'summary'       : '',
    'sequence'      : 1,
    'description'   : """
v1.1
----
*make sure we cannot create new data to certain field

*Nur Inayati
                      """,
    'category'      : 'laris custom ',
    'depends'       : ["purchase","account"],
    'data'          : [
                        "prevent_create_new.xml"
                      ],
    'installable'   : True,
    'application'   : True,
    'auto_install'  : False
}